#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <iostream>
#include <vector>
#include <thread>
#include <queue>


class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	void login(std::string name, SOCKET clientSocket);
	//void updateUser(SOCKET clientSocket);
	void newMsg(int lenName, SOCKET clientSocket);
	void sendMsg();
	std::string readFile(std::string file);

private:

	void accept();
	void clientHandler(SOCKET clientSocket);
	std::string getAllUsers();
	void writeToFile(std::string msg);
	SOCKET findSocket(std::string name);

	SOCKET _serverSocket;
	std::map<SOCKET, std::string> _clients;
	std::vector <std::thread> _threads;
	static std::queue<std::string> _messages;
};

