#include "Server.h"
#include "Helper.h"
#include <exception>
#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <fstream>

std::mutex msgMutex;
std::condition_variable cond;
std::queue<std::string> Server::_messages = {};

Server::Server()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
		
}

/*
C'tor to the server class
input: none
output: none
*/
Server::~Server()
{
	try
	{
		closesocket(_serverSocket);
	}
	catch (...) {}
}

/*
soccet serve new clients
input: port number
output: none
*/
void Server::serve(int port)
{	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;
	while (true)
	{
		
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}

/*
function accepts new clients that came into the server
input: none
output: none
*/
void Server::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	std::cout << "Client accepted. Server and client can speak" << std::endl;
	// the function that handle the conversation with the client
	this->_threads.push_back(std::thread(&Server::clientHandler, this, client_socket));
	this->_threads.back().detach();
}

/*
function handle with single client
input: the socket of the client
output: none
*/
void Server::clientHandler(SOCKET clientSocket)
{
	int len = 0;
	std::string name = "";
	try
	{
		while (true)
		{
			int typeCode = Helper::getMessageTypeCode(clientSocket);
			switch (typeCode)
			{
			case MT_CLIENT_LOG_IN: //handle a login msg
				len = Helper::getIntPartFromSocket(clientSocket, 2);
				name = Helper::getStringPartFromSocket(clientSocket, len);
				login(name, clientSocket);
				break;

			case MT_CLIENT_UPDATE: //handle an update request
				len = Helper::getIntPartFromSocket(clientSocket, 2);
				if (len == 0) Helper::send_update_message_to_client(clientSocket, "", "", getAllUsers());
				else
				{
					newMsg(len, clientSocket);
				}
				break;
			}
		}
	}
	catch (const std::exception& e)//delete a user from the listif exception was made
	{			
		std::map<SOCKET, std::string>::iterator i = this->_clients.find(clientSocket);
		this->_clients.erase(i);
		closesocket(clientSocket);
		for (i = this->_clients.begin(); i != this->_clients.end(); i++)
		{
			Helper::send_update_message_to_client(i->first, "", "", getAllUsers());
		}
		std::cout << getAllUsers() << std::endl;
	}
}

/*
functoin get all the user and return it in a string
input: none
output: a string of all the users
*/
std::string Server::getAllUsers()
{
	std::string names = "";
	std::map<SOCKET, std::string>::iterator i;
	for (i = this->_clients.begin(); i != this->_clients.end(); i++)
	{
		names += i->second;
		names += "&";
	}
	return names.substr(0, names.size() - 1);;
}

/*
function login a user into the app
input: name of the user, the socket of the user
output: none
*/
void Server::login(std::string name, SOCKET clientSocket)
{
	this->_clients[clientSocket] = name;	
	std::map<SOCKET, std::string>::iterator i;
	for (i = this->_clients.begin(); i != this->_clients.end(); i++)
	{
		Helper::send_update_message_to_client(i->first, "", "", getAllUsers());
	}
}

/*
function handle a new msg that a user wants to send
input: len if the user name, the socket of the user
output: none 
*/
void Server::newMsg(int lenName, SOCKET clientSocket)
{
	std::string name = Helper::getStringPartFromSocket(clientSocket, lenName);
	int lenMsg = Helper::getIntPartFromSocket(clientSocket, 5);
	std::string msg = Helper::getStringPartFromSocket(clientSocket, lenMsg);
	if (lenMsg != 0)
	{
		msgMutex.lock();
		this->_messages.push(Helper::getPaddedNumber(lenName, 2) + name + Helper::getPaddedNumber(this->_clients[clientSocket].size(), 2) + this->_clients[clientSocket] + Helper::getPaddedNumber(lenMsg, 5) + msg);		
		msgMutex.unlock();
		cond.notify_all();
		cond.notify_one();
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

/*
pop the last msg
input: none
output: none
*/
void Server::sendMsg()
{
	//initiate vars
	std::string msg = "";	
	while (true)
	{
		std::unique_lock<std::mutex> locker(msgMutex);
		cond.wait(locker, []() {return !_messages.empty(); });
		msg = this->_messages.front(); //read the first msg in line in the queue
		this->_messages.pop();
		writeToFile(msg);
		locker.unlock();		
	}
}

/*
function write the msg into a file
input: string of the msg
output: none
*/
void Server::writeToFile(std::string msg)
{
	std::ofstream outputFile;
	std::string names[2] = {};

	int lenName1 = stoi(msg.substr(0, 2));
	names[0] = msg.substr(2, lenName1);
	int lenName2 = stoi(msg.substr(lenName1 + 2, 2));
	names[1] = msg.substr(lenName1 + 4, lenName2);
	int lenMsg = stoi(msg.substr(lenName1 + 4 + lenName2, 5));
	std::string msgContent = msg.substr(lenName1 + 9 + lenName2, lenMsg);
	std::string author = names[1];
	std::string getter = names[0];
	std::string fileName = (names[0] < names[1] ? names[0] : names[1]) + "&" + (names[1] > names[0] ? names[1] : names[0]) + ".txt";
	outputFile.open(fileName, std::ios::app); //open the file for the output
	outputFile << "&MAGSH_MESSAGE&&Author&" + author + "&DATA&" + msgContent + "\n";
	outputFile.close();

	std::string content = readFile(fileName);
	Helper::send_update_message_to_client(findSocket(author), content, getter, getAllUsers());
	Helper::send_update_message_to_client(findSocket(getter), content, author, getAllUsers());
}

/*
function read a file and return a string of the content
input : string of the name of the file
output: string of the content
*/
std::string Server::readFile(std::string file)
{
	std::ifstream f(file);
	std::string content = "";
	std::string line;

	while (std::getline(f, line))
	{
		content += line;
	}
	return content;
}

SOCKET Server::findSocket(std::string name)
{
	std::map<SOCKET, std::string>::iterator i;
	for (i = this->_clients.begin(); i != this->_clients.end(); i++)
	{
		if (i->second == name) return i->first;
	}
	return NULL;
}