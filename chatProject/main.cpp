#pragma comment (lib, "ws2_32.lib")
#include <iostream>
#include "Server.h"
#include "WSAInitializer.h"
#include "Helper.h"

int main(void)
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;
		std::thread t(&Server::sendMsg, &myServer);
		t.detach();

		myServer.serve(8826);
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}